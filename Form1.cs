﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

namespace UserTypesEditor
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// 設定ファイルのパス
        /// </summary>
        public string Filename { get; set; } = @"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\usertype.dat";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            toolStripStatusLabel1.Text = Filename;

            // ロードイベント
            Load += ( s, e ) => { ReadSetting(); };
        }

        /// <summary>
        /// 設定ファイル読み込み処理
        /// </summary>
        /// <returns></returns>
        bool ReadSetting()
        {
            var status = false;

            // ファイルが無い場合
            if( File.Exists( Filename ) == false )
            {
                status = false;
            }
            else
            {
                var txt = File.ReadAllText( Filename );
                toolStripStatusLabel2.Text = $"{txt.Split( new[] { Environment.NewLine }, StringSplitOptions.None ).Count()} 行";

                textBox1.Text = txt;
                textBox1.SelectionStart = 0;
            }
            return status;
        }

        /// <summary>
        /// ボタンクリック処理(保存)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click( object sender, EventArgs e )
        {
            var q = from txt in textBox1.Text.Split( new[] { Environment.NewLine }, StringSplitOptions.None )
                    where txt.Length > 0
                    select txt;

            // ハッシュ変換（重複削除）
            var h = new HashSet<string>( q.ToArray() );
            // リストをソート
            var s = new SortedSet<string>( h.ToArray() );
            // テキスト化
            var t = string.Join( Environment.NewLine, s.ToArray() );

            File.WriteAllText( Filename, t );
            this.Close();
        }
    }
}
